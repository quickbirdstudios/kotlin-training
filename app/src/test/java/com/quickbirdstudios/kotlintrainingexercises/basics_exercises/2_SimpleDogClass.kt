package com.quickbirdstudios.kotlinbasics.exercises

import org.junit.Test


/**
 * Created by Malte Bucksch on 05/11/2017.
 */
class SimpleDogClassTests {
    // TODO TASK 1 add a constructor which initializes two properties "name" and "type"(read-only)
    // TODO TASK 2 add a function "bark" with a "times"(Int) argument -> which prints "wuff" multiple times
    // HINT: Use the "repeat" function ("xxx".repeat(3))
    // HINT: Use "println" function to print sth (println("test"))
    private class Dog() {
        // TODO TASK 3 add a private property "description" of type String
        // TODO TASK 4 add an "init" block which sets a description for the dog (combination of "type" and "name")

    }

    @Test
    fun testMyNewDogClass() {
        // TODO TASK 4: 1. create a dog instance named "Bruno" with type "Labrador"
        // TODO TASK    2. let him "bark" 3 times

        TODO("Please implement the task :-)")
    }
}
