package com.quickbirdstudios.calculator

/**
 * Created by Malte Bucksch on 12/12/2017.
 */

class MainViewModel {
//    TODO TASK parse the input string and calculate the result of the input formula
//    TODO TASK if input is invalid, return null
//    TODO TASK example formulas that should work:
//    TODO GOOD: "1+1","2-1", "3/4", "3*4"
//    TODO EVEN BETTER: "1+1*3","2*5+5" (first ignoring "Punkt vor Strich" and ignoring "/" since it leads to floating point numbers)
//    TODO WOW!: "1+1*3","2*5+5" (considering "Punkt vor Strich" - and optionally ignoring "/" since it leads to floating point numbers)
//    TODO INCREDIBLE: "1+1*3/4","2*5+5+5/6"  (considering "Punkt vor Strich")

    fun onMathFormulaInputChanged(input: String): Float? {
        return null
    }

}
